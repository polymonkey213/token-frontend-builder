export default {
	"projectName": "PolyDino",
	"projectSlogan": "Experience a new generation of jurasic DeFi",
	"tokenName": "Dino",
	"tokenSymbol": "DINO",
	"tokenContract":"0xf04810169dF6D14489788eeaFB45C0C1C3922F2F",
	"masterChefContract":"0xA3059E10ed2417A7E71Dfda3c805A850E79C4758",
	"projectTwitterHandle": "polydino_fi",
	"projectTwitter": "https://twitter.com/polydino_fi",
	"twitterTitle": "🦖 PolyDino - A revolutionary Farming with High Security on Polygon Network",
	"projectTelegram": "https://t.me/PolyDinoFinance",
	"projectReddit": "https://www.reddit.com/user/polydinofinance",
	"projectDocs": "https://polydino.gitbook.io/polydino-finance/",
	"projectGithub": "https://github.com/polydinofinance/polydino-contracts",
	"projectBlog": "https://polydinofi.medium.com/",
	"projectRoadmap":"https://polydino.gitbook.io/polydino-finance/information/roadmap",
	"addLiquididyURL":"https://quickswap.exchange/#/add/0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174/0x8a0712ec372732eeddb2a48e65782e83c6252b8c",
	"buyURL": "https://quickswap.exchange/#/swap?outputCurrency=0x8a0712ec372732eeddb2a48e65782e83c6252b8c",
	"dappRadarURL" : "https://dappradar.com/polygon/defi/polydino-finance",
	"dappComURL" : "https://www.dapp.com/app/polydino-network",
	"lpTokens":
	{
		"token-wmatic":"0xaf51f196ea6b0087ce46f3c527f7acfaff18b041",
		"token-usdc":"0x8a7c6f7527f557cbac0a299d00e51c9ac473f406",
	},
	"webTexts":
	{
		"farmsStatus":"Farms and Pools are being prepared"
	}
}