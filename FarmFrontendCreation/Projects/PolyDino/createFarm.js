const fs = require('fs');
const fse = require('fs-extra');

const targetDir = "./output/"

if (!fs.existsSync(targetDir)){
    fs.mkdirSync(targetDir)
}
const srcDir = "./../../Template/";
const destDir = targetDir;

console.log(" srcDir: ", srcDir)
console.log(" targetDir: ", destDir)

const filterFunc = (src, dest) => {
    let result = true;
    if(src.includes("node_modules"))
    {
        result = false;
    }

    return result;
}

                              
// To copy a folder or file  
fse.copySync(srcDir, destDir ,{ overwrite: true ,  dereference: true, filter: filterFunc } , function (err) {
  if (err) {                 
    console.error(err);
  } else {
    console.log("success!");
  }
});

// To asset folder
fse.copySync("./data/assets/", destDir+"/public" ,{ overwrite: true ,  dereference: true, filter: filterFunc } , function (err) {
  if (err) {                 
    console.error(err);
  } else {
    console.log("success!");
  }
});

fs.copyFileSync("./data/projectSettings.ts", targetDir + "src/projectSettings.ts",fs.constants.COPYFILE_FICLONE, (err) => {
  if (err) throw err;
  console.log('source.txt was copied to destination.txt');
});

fs.copyFileSync("./data/projectSettings.ts", targetDir + "src/projectSettings.ts",fs.constants.COPYFILE_FICLONE, (err) => {
  if (err) throw err;
  console.log('source.txt was copied to destination.txt');
});

fs.copyFileSync("./data/constants/farms.ts", targetDir + "src/config/constants/farms.ts",fs.constants.COPYFILE_FICLONE, (err) => {
  if (err) throw err;
  console.log('source.txt was copied to destination.txt');
});


fs.copyFileSync("./data/constants/pools.ts", targetDir + "src/config/constants/pools.ts",fs.constants.COPYFILE_FICLONE, (err) => {
  if (err) throw err;
  console.log('source.txt was copied to destination.txt');
});