import React, { useContext } from 'react'
import ProjectSettings from "projectSettings"
import { useWallet } from '@binance-chain/bsc-use-wallet'
import { allLanguages } from 'config/localisation/languageCodes'
import { LanguageContext } from 'contexts/Localisation/languageContext'
import useTheme from 'hooks/useTheme'
import { usePriceCakeBusd } from 'state/hooks'
import { Menu as UikitMenu } from '@pancakeswap-libs/uikit'
import config from './config'
import footerSocialsData from './footerSocials'

const Menu = (props) => {
  const { account, connect, reset } = useWallet()
  const { selectedLanguage, setSelectedLanguage } = useContext(LanguageContext)
  const { isDark, toggleTheme } = useTheme()
  const cakePriceUsd = usePriceCakeBusd()

  return (
    <UikitMenu
      account={account}
      login={connect}
      logout={reset}
      isDark={isDark}
      toggleTheme={toggleTheme}
      currentLang={selectedLanguage && selectedLanguage.code}
      langs={allLanguages}
      setLang={setSelectedLanguage}
      cakePriceUsd={cakePriceUsd}
      links={config}
      footerSocials={footerSocialsData}
      // priceLink="https://explorer-mainnet.maticvigil.com/token/0x8a0712ec372732eeddb2a48e65782e83c6252b8c"
      priceLink={"https://quickswap.exchange/#/swap?outputCurrency=".concat(ProjectSettings.tokenContract)}
     
      {...props}
    />
  )
}

export default Menu
