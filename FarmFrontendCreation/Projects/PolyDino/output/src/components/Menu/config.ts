import ProjectSettings from "projectSettings"
import { MenuEntry } from '@pancakeswap-libs/uikit'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Quickswap - Trade',
        // href: '/buy',
        href: "https://quickswap.exchange/#/swap?outputCurrency=".concat(ProjectSettings.tokenContract)
      },
      {
        label: 'Quickswap - Liquidity',
        // href: '/liquidity',
        href: "https://quickswap.exchange/#/add/0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174/".concat(ProjectSettings.tokenContract),
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Pools',
    icon: 'PoolIcon',
    href: '/pools',
  },
/*
  {
    label: 'Jungle Pools',
    icon: 'PoolIcon',
    href: '/junglepools',
  },
  {
    label: 'Launchpad (IDO)',
    icon: 'IfoIcon',
    href: '/ido',
  },
  {
    label: 'NFT Marketplace',
    icon: 'NftIcon',
    href: '/nft',
  },
  */
  {
    label: 'Info',
    icon: 'InfoIcon',
    items: [
      {
        label: 'QuickSwap Chart',
        href: 'https://info.quickswap.exchange/token/'.concat(ProjectSettings.tokenContract),
      },
      {
        label: 'Polygon Explorer',
        href: 'https://explorer-mainnet.maticvigil.com/token/'.concat(ProjectSettings.tokenContract),
      },
      /*
      {
        label: 'Dapp Radar',
        href: ProjectSettings.dappRadarURL
      },
      {
        label: 'Dapp.com',
        href: ProjectSettings.dappComURL
      },
      */
      {
        label: ProjectSettings.tokenSymbol.concat(' Chart'),
        href: 'https://quickchart.app/token/'.concat(ProjectSettings.tokenContract)
      }
    ],
  },
  {
    label: 'More',
    icon: 'MoreIcon',
    items: [
      {
        label: "Github",
        href: ProjectSettings.projectGithub,
      },
      /*
      {
        label: "Blog",
        href: ProjectSettings.projectBlog,
      },
      */
      {
        label: "Docs",
        href: ProjectSettings.projectDocs,
      },
    ],
  },
  /*
  {
    label: "Roadmap",
    icon: "RoadmapIcon",
    href: ProjectSettings.projectRoadmap,
  },
  {
    label: "Collab",
    icon: "HandshakeIcon",
    href: ProjectSettings.projectTelegram,
  }
  */
]

export default config
