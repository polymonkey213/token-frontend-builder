export default {
	"projectName": "PolyDino",
	"projectSlogan": "Experience a new generation of jurasic DeFi",
	"tokenName": "Dino",
	"tokenSymbol": "DINO",
	"tokenContract":"0x3BE698b235026368C35AaD14c08972bc5c5f2849",
	"masterChefContract":"0x3048BF1932449C98DE4b242Ad03b66C07394B2ce",
	"projectTwitterHandle": "polydino_fi",
	"projectTwitter": "https://twitter.com/polydino_fi",
	"twitterTitle": "🦖 PolyDino - A revolutionary Farming with High Security on Polygon Network",
	"projectTelegram": "https://t.me/polydino_fi",
	"projectReddit": "https://www.reddit.com/user/polydinofinance",
	"projectDocs": "https://polydino.gitbook.io/polydino-finance/",
	"projectGithub": "https://github.com/polydinofinance",
	"projectBlog": "https://polydinofi.medium.com/",
	"projectRoadmap":"https://polydino.gitbook.io/polydino-finance/information/roadmap",
	"addLiquididyURL":"https://quickswap.exchange/#/add/0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174/0x8a0712ec372732eeddb2a48e65782e83c6252b8c",
	"buyURL": "https://quickswap.exchange/#/swap?outputCurrency=0x8a0712ec372732eeddb2a48e65782e83c6252b8c",
	"dappRadarURL" : "https://dappradar.com/polygon/defi/polydino-finance",
	"dappComURL" : "https://www.dapp.com/app/polydino-network",
	"lpTokens":
	{
		"token-wmatic":"",
		"token-usdc":"",
	},
	"webTexts":
	{
		"farmsStatus":"Farms and Pools are being prepared"
	}
}