import { Card, CardBody, Heading, Text } from '@pancakeswap-libs/uikit'
import BigNumber from 'bignumber.js/bignumber'
import useI18n from 'hooks/useI18n'
import { useBurnedBalance, useTotalSupply } from 'hooks/useTokenBalance'
import ProjectSettings from "projectSettings"
import React from 'react'
import styled from 'styled-components'
import { getCakeAddress } from 'utils/addressHelpers'
import { getBalanceNumber } from 'utils/formatBalance'
import { useFarms, usePriceCakeBusd } from '../../../state/hooks'
import CardValue from './CardValue'

const StyledCakeStats = styled(Card)`
  margin-left: auto;
  margin-right: auto;
`

const Row = styled.div`
  align-items: center;
  display: flex;
  font-size: 14px;
  justify-content: space-between;
  margin-bottom: 8px;
`

const CakeStats = () => {
  const TranslateString = useI18n()
  const totalSupply = useTotalSupply()
  const burnedBalance = useBurnedBalance(getCakeAddress())
  const farms = useFarms();
  const eggPrice = usePriceCakeBusd();
  const circSupply = totalSupply ? totalSupply.minus(burnedBalance) : new BigNumber(0);
  const cakeSupply = getBalanceNumber(circSupply);
  const marketCap = eggPrice.times(circSupply);
  let tokenPerBlock = 0;
  const burnValue = eggPrice.times(burnedBalance);
  const cakePrice = usePriceCakeBusd();
  if(farms && farms[0] && farms[0].tokenPerBlock){
    tokenPerBlock = new BigNumber(farms[0].tokenPerBlock).div(new BigNumber(10).pow(18)).toNumber();
  }

  return (
    <StyledCakeStats>
      <CardBody>
        <Heading size="xl" mb="24px">
          { ProjectSettings.tokenSymbol.concat(' Stats')}
        </Heading>
        {/*
        <Row>
          <Text fontSize="14px">{ProjectSettings.tokenSymbol} Price</Text>
          <CardValue fontSize="14px" value={eggPrice.toNumber()} decimals={2} prefix="$" />
        </Row>
        
        */}
        <Row>
          <Text fontSize="14px">{TranslateString(999, 'Market Cap')}</Text>
          <CardValue fontSize="14px" value={getBalanceNumber(marketCap)} decimals={0} prefix="$" />
        </Row>
        <Row>
          <Text fontSize="14px">Total Minted</Text>
          {totalSupply && <CardValue fontSize="14px" value={getBalanceNumber(totalSupply)} decimals={0} />}
        </Row>
        <Row>
          <Text fontSize="14px">{TranslateString(538, 'Total '.concat(ProjectSettings.tokenSymbol).concat(' Burned'))}</Text>
          <CardValue fontSize="14px" value={getBalanceNumber(burnedBalance)} decimals={0} />
        </Row>
        <Row>
          <Text fontSize="14px">Circulating Supply</Text>
          {cakeSupply && <CardValue fontSize="14px" value={cakeSupply} decimals={0} />}
        </Row>
        <Row>
          <Text fontSize="14px">Maximum Supply</Text>
          <Text bold fontSize="14px">64,000</Text>
        </Row>
        {/*
        <Row>
          <Text fontSize="14px">Burn Value</Text>
          <CardValue fontSize="14px" value={getBalanceNumber(burnValue)} decimals={0} prefix="$" />
        </Row>
        */}
        <Row>
          <Text fontSize="14px">{ 'New '.concat(ProjectSettings.tokenSymbol).concat('/block')}</Text>
          <Text bold fontSize="14px">{tokenPerBlock}</Text>
        </Row>
      </CardBody>
    </StyledCakeStats>
  )
}

export default CakeStats
