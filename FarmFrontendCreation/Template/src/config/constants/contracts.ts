import ProjectSettings from "projectSettings"

export default {
  
  // Modify this two

  cake: {
    56: '0xF952Fc3ca7325Cc27D15885d37117676d25BfdA6',
    97: '',
    137: ProjectSettings.tokenContract, // TOKEN
  },
  masterChef: {
    56: '0xe70E9185F5ea7Ba3C5d63705784D8563017f2E57',
    97: '',
    137: ProjectSettings.masterChefContract, // Masterchef
  },

  // Network tokens, no need to modify

  wbnb: {
    56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
    97: '',
    137: '0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270', // WMATIC
    8001: '0x9c3C9283D3e44854697Cd22D3Faa240Cfb032889' // WMATIC Testnet
  },
  wmatic: {
    56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
    97: '',
    137: '0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270', // WMATIC
    8001: '0x9c3C9283D3e44854697Cd22D3Faa240Cfb032889' // WMATIC Testnet
  },
  lottery: {
    56: '',
    97: '',
  },
  lotteryNFT: {
    56: '',
    97: '',
  },
  mulltiCall: {
    56: '0x1ee38d535d541c55c9dae27b12edf090c608e6fb',
    97: '0x67ADCB4dF3931b0C5Da724058ADC2174a9844412',
    137: '0x95028E5B8a734bb7E2071F96De89BABe75be9C8E',
    8001: '0xa44b2D95e34f18b3998e6CC4Ccbc3bFa0aa130cE'
  },
  busd: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '',
    137: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174' // USDC
  },
  usdc: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '',
    137: '0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174' // USDC
  },
  usdt: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '',
    137: '0xc2132D05D31c914a87C6611C10748AEb04B58e8F' // USDT
  },
  weth: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '',
    137: '0x7ceb23fd6bc0add59e62ac25578270cff1b9f619' // WETH
  },
  forest: {
    56: '',
    97: '',
    137: '0x4b17699c4990265D35875C15D5377571159f6bfd'
  },
  wbtc: {
    56: '',
    97: '',
    137: '0x1bfd67037b42cf73acf2047067bd4f2c47d9bfd6'
  },
  dai: {
    56: '',
    97: '',
    137: '0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063'
  },
  quick: {
    56: '',
    97: '',
    137: '0x831753dd7087cac61ab5644b308642cc1c33dc13'
  },

  lp:{
    'wmatic-usdc': {
      56: '',
      97: '',
      137: '0x6e7a5FAFcec6BB1e78bAE2A1F0B612012BF14827'
    },
    'weth-usdc': {
      56: '',
      97: '',
      137: '0x853ee4b2a13f8a742d64c8f088be7ba2131f670d'
    },
    'wbtc-usdc': {
      56: '',
      97: '',
      137: '0xf6a637525402643b0654a54bead2cb9a83c8b498'
    },
    'dai-usdc': {
      56: '',
      97: '',
      137: '0xf04adBF75cDFc5eD26eeA4bbbb991DB002036Bdd'
    },
    'usdt-usdc': {
      56: '',
      97: '',
      137: '0x2cf7252e74036d1da831d11089d326296e64a728'
    },
    'usdc-usdc': {
      56: '',
      97: '',
      137: '0x2cf7252e74036d1da831d11089d326296e64a728'
    },
    'quick-usdc': {
      56: '',
      97: '',
      137: '0x23D265C88791c2e675d2bbC6E43E1E4634aC7101'
    },
    
  }
}
