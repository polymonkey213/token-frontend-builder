const fs = require('fs');
const fse = require('fs-extra');
const prompt = require('prompt-sync')();

const ProjectName = prompt('Project Name: ');
const ProjectDir = "./Projects/" + ProjectName
if (!fs.existsSync(ProjectDir))
{
  fs.mkdirSync(ProjectDir)
  fs.mkdirSync(ProjectDir+"/data")
  fs.mkdirSync(ProjectDir+"/data/assets")
  fs.mkdirSync(ProjectDir+"/data/assets/images")
  fs.mkdirSync(ProjectDir+"/data/constants")
  fse.copySync("./Template/public/images", ProjectDir+"/data/assets/images" ,{ overwrite: true ,  dereference: true})
  fse.copySync("./Template/public/Font.ttf", ProjectDir+"/data/assets/Font.ttf" ,{ overwrite: true ,  dereference: true})
  fse.copySync("./Template/public/favicon.ico", ProjectDir+"/data/assets/favicon.ico" ,{ overwrite: true ,  dereference: true})
  fse.copySync("./Template/src/projectSettings.ts", ProjectDir+"/data/projectSettings.ts" ,{ overwrite: true ,  dereference: true})
  fse.copySync("./Template/src/config/constants/farms.ts", ProjectDir+"/data/constants/farms.ts" ,{ overwrite: true ,  dereference: true})
  fse.copySync("./Template/src/config/constants/pools.ts", ProjectDir+"/data/constants/pools.ts" ,{ overwrite: true ,  dereference: true})
  fse.copySync("./farmCreatorTemplate.js", ProjectDir+"/createFarm.js" ,{ overwrite: true ,  dereference: true})
}
else
{
  console.error("Project directory already exists!")
}
