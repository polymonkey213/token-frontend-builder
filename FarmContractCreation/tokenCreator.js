const fs = require('fs');
const prompt = require('prompt-sync')();
const _ = require('lodash')

const TemplateMasterChef = fs.readFileSync("./templates/TemplateMasterChef.sol").toString()
const TemplateToken = fs.readFileSync("./templates/TemplateToken.sol").toString()
const TemplateTimelock = fs.readFileSync("./templates/TemplateTimelock.sol").toString()


const ContractName = prompt('Contract Name: ');
const ContractSymbol = ContractName.toUpperCase()
const ContractLower = ContractName.toLowerCase()

var ContractToken = TemplateToken.replace(new RegExp("<TOKEN_ID>","g"),ContractSymbol);
ContractToken = ContractToken.replace(new RegExp("<TOKEN_Upper>","g"),ContractName);
ContractToken = ContractToken.replace(new RegExp("<TOKEN_Lower>","g"),ContractLower);

var ContractMasterChef = TemplateMasterChef.replace(new RegExp("<TOKEN_ID>","g"),ContractSymbol);
ContractMasterChef = ContractMasterChef.replace(new RegExp("<TOKEN_Upper>","g"),ContractName);
ContractMasterChef = ContractMasterChef.replace(new RegExp("<TOKEN_Lower>","g"),ContractLower);

const outputDir = "./output/"+ContractName+"/"

if (!fs.existsSync(outputDir)){
    fs.mkdirSync(outputDir, { recursive: true });
}

const ContractFileName = ContractName + "Token.sol"
const MasterChefFileName = ContractName + "MasterChef.sol"
const TimelockFileName = ContractName + "Timelock.sol"

fs.writeFileSync(outputDir+ContractFileName,ContractToken);
fs.writeFileSync(outputDir+MasterChefFileName,ContractMasterChef);
fs.writeFileSync(outputDir+TimelockFileName,TemplateTimelock);